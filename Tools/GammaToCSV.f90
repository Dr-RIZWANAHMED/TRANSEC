
program GammaCSV

  integer, parameter :: dp = kind(1.0d0)
  integer, parameter :: dpc = kind((1.0d0,1.0d0))
  complex(dpc), parameter :: zzero = cmplx(0.d0, 0.d0)

  integer :: NE 

  integer :: nstate, alcstat, i, j, k, nstateUse
  complex(dpc), dimension (:,:), pointer ::  GamLR  
!  complex(dpc), allocatable, dimension(:) :: eval
  complex(dpc) TE
  real(dp) :: Emin, Emax, dE, E


  print *, 'Nstate? '
  read *, nstate
  print *, 'Nstate Use?'
  read *, nstateUse

! Allocate memory for CAPs GamL, GamR in diagonal basis of (H + i Gamma)
  allocate(GamLR(nstate,nstate), stat=alcstat) 
!  call alccheck('gamlr', nstate**2, alcstat)

! Allocate memory for eigenvalues of (H + i Gamma)
!  allocate(eval(nstate), stat=alcstat) 
!  call alccheck('eval', nstate, alcstat)


  GamLR(:,:) = zzero
!  eval(:) = zzero


    open(88, file='GammaLR.dat', form='unformatted', status='old')
    read(88) GamLR 
    close(88)

!    open(88, file='Eval.dat', form='unformatted', status='old')
!    read(88) eval
!    close(88) 



	   ! Write CAPs in complex eigenbasis to disk

	open(1001,file='GammaLR.csv',form='formatted')
           do i=1, nstateUse
             do j=1, nstateUse
           write(1001,*) GamLR(i,j)
           call flush(1001)
             enddo
           enddo
	   close(1001)
           

      if (associated (GamLR)) deallocate(GamLR)
!      deallocate(eval)


end program GammaCSV  


