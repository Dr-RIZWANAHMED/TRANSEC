#! /home/sfzhu/anaconda3/bin/python

# Script by Shifeng Zhu, May 2021
# This script is to import the TE dat and export them to the CSV file.
# python3 "TE.TOT*dat" csv_filename
import re, sys, csv
import numpy as np

# This part is to sort the file names based on their index, not by alphabetical order.
csv_filename = "kpts_data.csv"
if(len(sys.argv) >= 3):
    csv_filename = sys.argv[2] + ".csv"

output_file  = sys.argv[1]

if (not output_file):
    print("\nFile not found")
    print("Usage: <python> generate_kpts_CSV.py 'parsec.out', where parsec.out is a string (note the single quotes) representing the output files.\n")
    exit()

bohr =  0.5291772109038
real_space_field_names = ['Lattice vectors [bohr]', '','', '', 'Lattice vectors [A]', '',''] 
reci_space_field_names = ['Reciprocal space vectors [bohr^(-1)]', '','', '', 'Reciprocal space vectors [A^(-1)]', '',''] 
kpts_field_names = ['Kpt index', 'weigth', 'relative a', 'relative b', 'kx [A^(-1)]', 'ky [A^(-1)]', '|k| [A^(-1)]']
real_space_data = []
reci_space_data = []
kpts_data = []
### This part is to try to match the kpts relative coordinate
num_kpts_pattern = re.compile('(?P<num_kpts_from_p>\d+) K points generated from Monkhorst-Pack grid :')
num_kpts = -1
kpts_starting_line = -1
kpts_ending_line = -1
### This part is to match unit lattice vector
coordinates_pattern = re.compile('Unit lattice vectors \[bohr\] :')
coordinates_starting_line = -1
coordinates_ending_line = -1
ax = 0.0
ay = 0.0
bx = 0.0
by = 0.0
i_ax = 0.0
i_ay = 0.0
i_bx = 0.0
i_by = 0.0

with open(output_file, 'r') as fObj:
    for i, line in enumerate(fObj, 1):
        line = line.strip()
        if(kpts_starting_line == -1):
            m = num_kpts_pattern.match(line)
            if m:
                kpts_starting_line = i +  5
                num_kpts = int(m.group('num_kpts_from_p'))
                kpts_ending_line = kpts_starting_line + num_kpts - 1
        elif i >= kpts_starting_line and i <= kpts_ending_line:
            cur_line_list = line.split()
            kpt_data =[]
            cur_kpt_index = int(cur_line_list[0])
            cur_kpt_weight = float(cur_line_list[1])
            cur_kpt_rel_a = float(cur_line_list[2])
            cur_kpt_rel_b = float(cur_line_list[3])
            cur_kpt_kx = cur_kpt_rel_a*i_ax + cur_kpt_rel_b*i_bx
            cur_kpt_ky = cur_kpt_rel_a*i_ay + cur_kpt_rel_b*i_by
            cur_kpt_abs_k = np.sqrt(cur_kpt_kx*cur_kpt_kx  + cur_kpt_ky*cur_kpt_ky)
            kpt_data.append(cur_kpt_index)
            kpt_data.append(cur_kpt_weight)
            kpt_data.append(cur_kpt_rel_a)
            kpt_data.append(cur_kpt_rel_b)
            kpt_data.append(cur_kpt_kx)
            kpt_data.append(cur_kpt_ky)
            kpt_data.append(cur_kpt_abs_k)
            kpts_data.append(kpt_data) 
        if(coordinates_starting_line == -1):
            m = coordinates_pattern.match(line) 
            if m:
                coordinates_starting_line = i + 1
                coordinates_ending_line   = i + 2
        elif i == coordinates_starting_line:
            cur_line_list = line.split()
            ax = float(cur_line_list[0])
            ay = float(cur_line_list[1])
        elif i == coordinates_ending_line:
            cur_line_list = line.split()
            bx = float(cur_line_list[0])
            by = float(cur_line_list[1])
            factor = 2*np.pi/(ax*by - ay*bx)
            i_ax =  by*factor
            i_ay = -bx*factor
            i_bx = -ay*factor
            i_by =  ax*factor
            real_space_data = [[ax, ay, 0, '', ax*bohr, ay*bohr, 0],
                               [bx, by, 0, '', bx*bohr, by*bohr, 0]]
            reci_space_data = [[i_ax, i_ay, 0, '', i_ax/bohr, i_ay/bohr, 0],
                               [i_bx, i_by, 0, '', i_bx/bohr, i_by/bohr, 0]]

with open(csv_filename, 'w') as f:
    write = csv.writer(f)
    
    write.writerow(real_space_field_names)
    write.writerows(real_space_data)
    write.writerow([])
    write.writerow(reci_space_field_names)
    write.writerows(reci_space_data)
    write.writerow([])
    write.writerow(kpts_field_names)
    write.writerows(kpts_data)
