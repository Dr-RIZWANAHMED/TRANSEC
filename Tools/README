Updated 7/2022, 12/2017  
6/2014
Baruch Feldman

CalcT - As decribed in the TRANSEC paper (see ../Paper/), the TRANSEC method can compute T(E) at additional energies E at very little additional computational cost.  CalcT is a post-processing utility to calculate T(E) given the GammaLR.dat, and Eval.dat files from a previous TRANSEC run.  This is very useful, for example to compute the T(E) curve over a new energy range different from the original TRANSEC run, or at a greater energy resolution.  Using CalcT, it is not necessary to re-run the full TRANSEC computation.  Another use is to re-compute T(E) using only a subset of the eigenpairs from the original run -- this is very useful for checking convergence of the calculation with respect to number of eigenpairs.  Note: the energy range over which you can obtain valid T(E) results is roughly the range of the complex eigenvalues solved by TRANSEC, i.e. between min_i{Re{epsilon_i}} and max_i{Re{epsilon_i}}.  
Usage:
When running, you can input parameters at the command line in interactive mode.  More typically, pipe the parameters from a text file such as in.txt into standard input, e.g. CalcT.e < in.txt.  See calct.scr for example.  
You must specify:
	i) The number of eigenpairs present in the *.dat files
	ii) The starting and ending energies and number of intermediate energies for T(E)
	iii) The number of eigenpairs to use.  This is <= the number of eigenpairs present (i).  
This utility is useful to get higher-resolution T(E) curves, to run over a different energy range, and to check convergence of # eigenpairs (by decreasing this number).  Also useful for various tests.  
Compilation: 
gfortran CalcT.f90 -o CalcT.e

CalcT_AllKpts.sh - This is a shell script to use CalcT with periodic boundary condition (PBC) calculations.  Usage: create a subdirectory below the directory where the PBC calculation has been run (i.e., the directory with Eval*dat and GammaLR*dat), copy CalcT_AllKpts.sh to the subdirectory, and edit line 3 of CalcT_AllKpts.sh to specify the correct number of k-points.  Then run CalcT_AllKpts.sh.  


MultiCalcT - Post-processing utility for use with "multi-"TRANSEC, i.e. for combining multiple TRANSEC runs over different energy ranges into a SINGLE GammaL.dat + GammaR.dat, as described in [Feldman and Zhou, Comp. Phys. Communic. 207, 105 (2016); arXiv:1606.01139].  
Notes: 
	1) This is an intermediate step, to combine Evec.dat from multiple TRANSEC runs into a single Gamma?.dat.  Therefore, it must be run BEFORE CalcT, and does NOT replace CalcT.  
	2) MultiCalcT eliminates linearly dependent eigenpairs from the multiple runs.  However, at present, it doesn't sort them & write a new Evec.dat, which would be useful for adding the new runs in stages.  
Usage:
As presently written, MultiCalcT works ONLY if ALL TRANSEC runs were done on the same number of cores.  (This is because Evec.dat is defined over the entire 3-D real-space grid, which is partitioned in a way that depends on the number of cores.)  
Typically pipe inMulti.txt to standard input.  See multicalct.scr for example. 
You must specify:
	i) The total number of grid points ("grep size parsec.out")
	ii) The number of runs being combined and # of eigenpairs in each run.  
	iii) A path to Evec.dat, Eval.dat, and CAP.dat (in that order) from each run.  
Compilation:
gfortran MultiCalcT.f90 -fopenmp -o MultiCalcT.mp


AvgTE.py - This is a post-processing utility for use with periodic boundary condition (PBC) calculations.  AvgTE.py simply averages the k-point-resolved T_k(E) curves (e.g., TE.TOT.Kpt*.dat) to obtain the average <T(E)>.  Note that AvgTE.py does not currently weight the gamma point differently than the other k-points, despite the fact that all other k-points include results for both +-k.  This is a known issue, but it usually does not make a significant quantitative difference in the average result.  Usage: <python> AvgTE.py 'fileID', where fileID is a string (note the single quotes) representing all T(E) files to average.  fileID can include a path and/or a wildcard pattern, for example: python AvgTE.py 'TE.*.dat' .  


generate_TE_to_CSV.py and generate_kpts_CSV.py - These scripts convert the TE*dat files to comma-separated-value (CSV) format, for the cases without PBCs and with PBCs, respectively.  This is useful for plotting and data analysis purposes.  Thanks to Shifeng Zhu for providing these scripts!  


Evalues.f90 and Evectors.f90 - These tools generate text files with the complex eigenvalues and eigenvectors computed by Transec, respectively,  for inspection, data analysis, or debugging purposes.  They look, respectively, for files named Eval.dat and Evec.dat, so you may need to copy or link to the files output by Transec.  Note that Transec only writes Evec*dat when the cond_write_evec keyword is specified as true in parsec.in.  
Compilation: gfortran Evalues.f90 -o Evalues.e ; gfortran Evectors.f90 -o Evectors.e


GammaToCSV.f90 - This tool converts the binary output file GammaLR.dat to comma-separated-value (CSV) format for inspection, data analysis, or debugging purposes.  
Compilation: gfortran GammaToCSV.f90 -o GammaToCSV.e  


Transec_Model_Matlab_Code.m - this is a simplified "model" of Transec written in Matlab.  It sets up a model 1-dimensional tight-binding Hamiltonian (the default size of H is 20x20), adds Gaussian CAPs, and fully diagonalizes to compute T(E).  



