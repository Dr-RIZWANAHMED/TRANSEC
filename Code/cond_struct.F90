! ===============================================================
! Copyright (C) 2005 Finite Difference Research Group
! This file is part of parsec, http://www.ices.utexas.edu/parsec/
! And copyright (C) 2011-2022 Baruch Feldman, with modifications for Transec 
!
!
! COND
! =================== conductance module ========================
module conductance_module
  use constants
  implicit none

  type gaussian_gamma_potential
    ! Peak amplitude, width, and peak position for a Gaussian complex 
	! absorbing potential (CAP)
    real(dp) :: height, stdev, z0
  end type gaussian_gamma_potential

  type conductance_param
    ! Flag for doing conductance calculations
    logical :: do_cond
    ! Flag for writing complex eigenvectors of (H + i Gamma) to disk 
    logical :: WriteEvectors 
    ! For use with lateral PBC k-points: flag denoting whether to perform automatic 
	! check for symmetry, which can override user settings if the symmetry is not found   
	logical :: CheckSymm  
    ! For use with lateral PBC k-points: flag denoting whether the system possesses 
    ! R_z,pi (rotation by pi about z axis, (x,y,z)->(-x,-y,z)) symmetry 
    ! If system lacks R_z,pi symmetry, we will solve explicitly for both +k and -k points, 
    ! as the CAPs break time-reversal invariance  
    logical :: RzSymm 

    ! the Gaussian gamma potentials
    type(gaussian_gamma_potential), dimension(2) :: gaussians

    ! Imaginary complex absorbing potentials (CAPs) gamma  
    ! for conductance calculations
    ! gammal, gammar : gamma left, gamma right
    ! gamma = gammal + gammar
    real(dp), dimension (:), pointer :: gammal
    real(dp), dimension (:), pointer :: gammar

    real(dp), dimension (:), pointer :: gammafull
    
	! Z coordinate shift for CAPs gamma  
	real(dp) :: zshift 
	
    ! conductance temperature
    real(dp) :: Tcond
    ! conductance post-SCF "external bias"
    real(dp) :: vbias
    ! threshold for cutting off the Fermi-Dirac distribution
    real(dp) :: fermcut
    ! Fermi level
    real(dp) :: efermi 
	
	! Constant shift in Hamiltonian, for purpose of multi-partition runs 
	complex(dpc) :: shift


    ! The number of complex eigenpairs (of H + i Gamma) to solve for in the conductance calculation
    integer :: NumEpairs
    real(dp) :: ProportionEpairs

    ! Tolerance for eigensolver (presently restricted to Arpack) in conductance module 
    real(dp) :: Eigensolution_tol 

    integer :: Restart
!    integer :: RestartFrom

    ! The range of eigenstates to solve for & include in the conductance calcs
    ! Only levels close to the Fermi level are needed
    real(dp) :: Emin, Emax
	
    ! number of energy points in the energy window
    integer :: ne

    ! energy step between two consecutive energy points in the window
    real(dp) :: de

    ! the injection energies where we calculate T(E)
    real(dp), dimension(:), pointer :: Espectrum

	! List of k-point indices to ignore (i.e., not solve) in the conductance calculation
	integer, dimension(:), pointer :: Ignore_KPoints 
	integer :: Num_Ignore_KPoints  ! Size of the Ignore_KPoints array  
	
	! The number of k-points that should be parallelized over the sign of k when R_z,pi symmetry is absent.  
	! Generally, the k-points that will be parallelized over sign will be those at the beginning of the k-point list, including the Gamma point.  
	integer :: Parallelize_Over_SignK  
	
    ! The number of MPI groups for the conductance calculation (negative value on user input: keep same groups used in DFT; 0: determine number of groups automatically; positive value: set this number of groups)  
    integer :: NumGroups

  end type conductance_param

contains
  subroutine destroy_conductance_param (cond)
    implicit none
    type (conductance_param), intent(inout) :: cond

    if(associated (cond%gammal)) deallocate(cond%gammal)
    if(associated (cond%gammar)) deallocate(cond%gammar)
    if(associated (cond%gammafull)) deallocate(cond%gammafull)
    if(associated (cond%Espectrum)) deallocate(cond%Espectrum)
    if(associated (cond%Ignore_KPoints)) deallocate(cond%Ignore_KPoints)
  end subroutine destroy_conductance_param

end module conductance_module
! END COND

! ============ End of conductance structure module ==============
! ===============================================================
